﻿using LendFoundry.Foundation.Services;
using LendFoundry.Merchant.Filters.Tests.Fakes;
using LendFoundry.Security.Identity.Client;
using LendFoundry.Security.Tokens;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace LendFoundry.Merchant.Filters.Tests
{
    public class MerchantFilterServiceTest
    {
        [Fact]
        public void GetAllWhenItHasData()
        {
            // creates a list of filter to me consumed
            var _merchantid = "MER0001";
            var filter = GetData();
            filter.MerchantId = _merchantid;
            var repo = new FakeRepository();
            repo.Add(filter);
            repo.Add(filter);
            repo.Add(filter);
            var service = BuildService(repo);

            // gets all filter from database
            var data = service.GetAll();

            Assert.NotNull(data);
            Assert.NotEmpty(data);
            Assert.Equal(3, data.Count());
        }

        [Fact]
        public void GetAllWhenItHasNoData()
        {
            // try gets filters from an empty database
            var service = BuildService();
            var data = service.GetAll();

            Assert.NotNull(data);
            Assert.Empty(data);
            Assert.Equal(0, data.Count());
        }

        [Fact]
        public void GetByStatusWhenItHasDataAndStatusCodeMatches()
        {
            // creates all entries to be consumed
            var _merchantid = "MER0001";
            var _status = "100.01";
            var filter = GetData();
            filter.MerchantId = _merchantid;
            filter.StatusCode = _status;

            var repo = new FakeRepository();
            repo.Add(filter);
            repo.Add(filter);
            repo.Add(filter);

            var service = BuildService(repo);
            var data = service.GetByStatus(_status);

            Assert.NotNull(data);
            Assert.NotEmpty(data);
            Assert.Equal(3, data.Count());
        }

        [Fact]
        public void GetByStatusWhenItHasDataButCodeDoNotMatches()
        {
            // creates all entries to be consumed
            var _merchantid = "MER0001";
            var _status = "100.01";
            var filter = GetData();
            filter.MerchantId = _merchantid;
            filter.StatusCode = _status;

            var repo = new FakeRepository();
            repo.Add(filter);
            repo.Add(filter);

            var service = BuildService(repo);
            var data = service.GetByStatus("100.02");

            Assert.NotNull(data);
            Assert.Empty(data);
            Assert.Equal(0, data.Count());
        }

        [Fact]
        public void GetByStatusWhenCodeIsInvalid()
        {
            Assert.Throws<ArgumentException>(() =>
            {
                var service = BuildService();
                service.GetByStatus("");
            });

            Assert.Throws<ArgumentException>(() =>
            {
                var service = BuildService();
                service.GetByStatus(string.Empty);
            });

            Assert.Throws<ArgumentException>(() =>
            {
                var service = BuildService();
                service.GetByStatus(" ");
            });
        }

        [Fact]
        public void GetAssignedByStatusWhenNullUsername()
        {
            var repository = new FakeRepository();

            // filter object 1
            var filter = GetData();
            filter.StatusCode = "100.01";
            repository.Add(filter);

            // filter object 2
            filter = GetData();
            filter.StatusCode = "100.02";
            repository.Add(filter);
            // creates a service and gets item to match
            var tokenParser = new Mock<ITokenHandler>();
            tokenParser.Setup(x => x.Parse(It.IsAny<string>()))
                .Returns(new Token { });

            var service = BuildService(repository, null, tokenParser.Object);

            Assert.ThrowsAsync<ArgumentException>(async () => await service.GetAssignedByStatus(new string[] { "SourceId3" }));
        }

        [Fact]
        public void GetAssignedByStatusWhenNullStatuses()
        {
            // creates a filter view entry in the repository   
            var repository = new FakeRepository();

            // filter object 1
            var filter = GetData();
            filter.StatusCode = "100.01";
            repository.Add(filter);

            // filter object 2
            filter = GetData();
            filter.StatusCode = "100.02";
            repository.Add(filter);
            // creates a service and gets item to match
            var tokenParser = new Mock<ITokenHandler>();
            tokenParser.Setup(x => x.Parse(It.IsAny<string>()))
                .Returns(new Token { Subject = "Sanchari" });

            var service = BuildService(repository, null, tokenParser.Object);

            Assert.ThrowsAsync<InvalidArgumentException>(async () => await service.GetAssignedByStatus(null));
            Assert.ThrowsAsync<InvalidArgumentException>(async () => await service.GetAssignedByStatus(new string[] { }));
        }

        [Fact]
        public void GetAssignedByStatusWhenValidStatuses()
        {
            // creates a filter view entry in the repository   
            var repository = new FakeRepository();

            // filter object 1
            var filter = GetData();
            filter.StatusCode = "100.01";
            filter.Assignees = new Dictionary<string, string> { ["Sanchari"] = "Sanchari" };

            repository.Add(filter);

            // filter object 2
            filter = GetData();
            filter.StatusCode = "100.02";
            filter.Assignees = new Dictionary<string, string> { ["Sanchari"] = "Sanchari" };
            repository.Add(filter);
            // creates a service and gets item to match
            var tokenParser = new Mock<ITokenHandler>();
            tokenParser.Setup(x => x.Parse(It.IsAny<string>()))
                .Returns(new Token { Subject = "Sanchari" });

            var service = BuildService(repository, null, tokenParser.Object);

            var result = Task.Run(async () => await service.GetAssignedByStatus(new string[] { "100.01" })).Result;

            Assert.NotNull(result);
            Assert.NotEmpty(result);
        }

        [Fact]
        public void GetAssignedByTagsWhenNullUsername()
        {
            // creates a filter view entry in the repository   
            var repository = new FakeRepository();

            // filter object 1
            var filter = GetData();
            filter.StatusCode = "100.01";
            filter.Tags = new string[] { "tag1" };
            repository.Add(filter);

            // filter object 2
            filter = GetData();
            filter.StatusCode = "100.02";
            filter.Tags = new string[] { "tag1" };
            repository.Add(filter);
            // creates a service and gets item to match
            var tokenParser = new Mock<ITokenHandler>();
            tokenParser.Setup(x => x.Parse(It.IsAny<string>()))
                .Returns(new Token { });

            var service = BuildService(repository, null, tokenParser.Object);

            Assert.ThrowsAsync<ArgumentException>(async () => await service.GetAssignedByTags(new string[] { "tag1" }));
        }

        [Fact]
        public void GetAssignedByTagsWhenNullTags()
        {
            // creates a filter view entry in the repository   
            var repository = new FakeRepository();

            // filter object 1
            var filter = GetData();
            filter.StatusCode = "100.01";
            filter.Tags = new string[] { "tag1" };
            repository.Add(filter);

            // filter object 2
            filter = GetData();
            filter.StatusCode = "100.02";
            filter.Tags = new string[] { "tag1" };
            repository.Add(filter);
            // creates a service and gets item to match
            var tokenParser = new Mock<ITokenHandler>();
            tokenParser.Setup(x => x.Parse(It.IsAny<string>()))
                .Returns(new Token { Subject = "Sanchari" });

            var service = BuildService(repository, null, tokenParser.Object);

            Assert.ThrowsAsync<InvalidArgumentException>(async () => await service.GetAssignedByTags(null));
            Assert.ThrowsAsync<InvalidArgumentException>(async () => await service.GetAssignedByTags(new string[] { }));
        }

        [Fact]
        public void GetAssignedByTagsWhenValidTags()
        {
            // creates a filter view entry in the repository   
            var repository = new FakeRepository();

            // filter object 1
            var filter = GetData();
            filter.StatusCode = "100.01";
            filter.Tags = new string[] { "tag1" };
            filter.Assignees = new Dictionary<string, string> { ["Sanchari"] = "Sanchari" };

            repository.Add(filter);

            // filter object 2
            filter = GetData();
            filter.StatusCode = "100.02";
            filter.Tags = new string[] { "tag1" };
            filter.Assignees = new Dictionary<string, string> { ["Sanchari"] = "Sanchari" };
            repository.Add(filter);
            // creates a service and gets item to match
            var tokenParser = new Mock<ITokenHandler>();
            tokenParser.Setup(x => x.Parse(It.IsAny<string>()))
                .Returns(new Token { Subject = "Sanchari" });

            var service = BuildService(repository, null, tokenParser.Object);

            var result = Task.Run(async () => await service.GetAssignedByTags(new string[] { "tag1" })).Result;

            Assert.NotNull(result);
            Assert.NotEmpty(result);
        }

        [Fact]
        public void GetAllAssignedApplicationsWhenNotUserFound()
        {
            Assert.Throws<ArgumentException>(() =>
            {
                var repository = new FakeRepository();
                var tokenParser = new Mock<ITokenHandler>();
                tokenParser.Setup(x => x.Parse(It.IsAny<string>())).Returns(new Token { });
                var service = BuildService(repository, null, tokenParser.Object);
                service.GetAllAssigned();
            });
        }

        [Fact]
        public void GetAllAssignedApplications()
        {
            var repository = new FakeRepository();
            var filter = GetData();
            filter.StatusCode = "100.01";
            filter.Tags = new string[] { "tag1" };
            filter.Assignees = new Dictionary<string, string> { ["Sanchari"] = "Sanchari" };

            repository.Add(filter);

            // filter object 2
            filter = GetData();
            filter.StatusCode = "100.02";
            filter.Tags = new string[] { "tag1" };
            filter.Assignees = new Dictionary<string, string> { ["Sanchari"] = "Sanchari" };
            repository.Add(filter);
            var tokenParser = new Mock<ITokenHandler>();
            tokenParser.Setup(x => x.Parse(It.IsAny<string>())).Returns(new Token { Subject = "Sanchari" });
            var service = BuildService(repository, null, tokenParser.Object);
            var result = Task.Run(async () => await service.GetAllAssigned()).Result;

            Assert.NotNull(result);
            Assert.NotEmpty(result);
        }
        [Fact]
        public void GetByStatusWhenItHasDataAndStatusMatches()
        {
            // creates all entries to be consumed
            var _merchantid = "MER0001";
            var _status = new string[] { "100.01"};
            var filter = GetData();
            filter.MerchantId = _merchantid;
            filter.StatusCode = "100.01";
            var repo = new FakeRepository();
            repo.Add(filter);
            repo.Add(filter);
            repo.Add(filter);

            var service = BuildService(repo);
            var data = service.GetByStatus(_status).Result;

            Assert.NotNull(data);
            Assert.NotEmpty(data);
            Assert.Equal(3, data.Count());
        }

        [Fact]
        public void GetByStatusWhenItHasDataButStatusNotMatches()
        {
            // creates all entries to be consumed
            var _merchantid = "MER0001";
            var _status = "100.01";
            var filter = GetData();
            filter.MerchantId = _merchantid;
            filter.StatusCode = _status;

            var repo = new FakeRepository();
            repo.Add(filter);
            repo.Add(filter);

            var service = BuildService(repo);
            var data = service.GetByStatus(new string[] { "100.02" }).Result;

            Assert.NotNull(data);
            Assert.Empty(data);
            Assert.Equal(0, data.Count());
        }

        [Fact]
        public void GetByStatusWhenStatusIsNullOrEmpty()
        {
            IEnumerable<string> statuses = new[] { "100.01", "100.02" };
            Assert.ThrowsAsync<InvalidArgumentException>(async () =>
            {
                var service = BuildService();
                statuses = null;
                await service.GetByStatus( statuses);
            });

            Assert.ThrowsAsync<InvalidArgumentException>(async () =>
            {
                var service = BuildService();
                statuses = Enumerable.Empty<string>();
                await service.GetByStatus(statuses);
            });

        }

        private IMerchantFilterService BuildService(IFilterViewRepository repository = null, ITokenReader tokenReader = null,
               ITokenHandler tokenHandler = null,
               IIdentityService identityService = null)
        {
            IFilterViewRepository inTimeRepository = repository;
            if (inTimeRepository == null)
                inTimeRepository = new FakeRepository();

            ITokenReader inTimeTokenReader = tokenReader;
            if (inTimeTokenReader == null)
                inTimeTokenReader = Mock.Of<ITokenReader>();

            ITokenHandler inTimeTokenHandler = tokenHandler;
            if (inTimeTokenHandler == null)
                inTimeTokenHandler = Mock.Of<ITokenHandler>();

            IIdentityService inIdentityService = identityService;
            if (inIdentityService == null)
                inIdentityService = Mock.Of<IIdentityService>();

            return new MerchantFilterService(inTimeRepository, inTimeTokenReader, inTimeTokenHandler, inIdentityService);
        }

        private IFilterView GetData()
        {
            return new FilterView
            {
                BusinessName = "BusinessName",
                City = "City",
                Contact = "Contact",
                Phone = "Phone",
                State = "State",
                StatusCode = "Status"
            };
        }
    }
}