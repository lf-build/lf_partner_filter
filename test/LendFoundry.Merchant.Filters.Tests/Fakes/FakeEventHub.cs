﻿using LendFoundry.EventHub.Client;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace LendFoundry.Merchant.Filters.Tests.Fakes
{
    public class FakeEventHub : IEventHubClient
    {
        public dynamic InMemoryData { get; set; }

        public void On(string eventName, Action<EventInfo> handler)
        {
            handler.Invoke(new EventInfo
            {
                Name = eventName,
                Data = InMemoryData
            });
        }

        public Task<bool> Publish<T>(T @event)
        {
            return Task.Run(() => true);
        }

        public Task<bool> Publish<T>(string eventName, T @event)
        {
            return Task.Run(() => true);
        }

        public void PublishBatchWithInterval<T>(List<T> events, int interval = 100)
        {
        }

        public void Start()
        {
        }

        public void StartAsync()
        {
        }

        public void Stop()
        {
            throw new NotImplementedException();
        }
    }
}