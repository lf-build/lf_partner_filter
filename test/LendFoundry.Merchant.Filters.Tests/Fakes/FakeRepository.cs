﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace LendFoundry.Merchant.Filters.Tests.Fakes
{
    public class FakeRepository : IFilterViewRepository
    {
        public List<IFilterView> InMemoryData { get; set; } = new List<IFilterView>();

        public void Add(IFilterView item)
        {
            item.Id = Guid.NewGuid().ToString("N");
            InMemoryData.Add(item);
        }

        public void Update(IFilterView item)
        {
            var index = InMemoryData.IndexOf(item);
            InMemoryData[index] = item;
        }

        public void AddOrUpdate(IFilterView view)
        {
            if (string.IsNullOrWhiteSpace(view.Id))
            {
                view.Id = Guid.NewGuid().ToString("N");
                InMemoryData.Add(view);
            }
            else
            {
                var index = InMemoryData.IndexOf(view);
                InMemoryData[index] = view;
            }
        }

        public Task<IEnumerable<IFilterView>> All(Expression<Func<IFilterView, bool>> query, int? skip = default(int?), int? quantity = default(int?))
        {
            return Task.Run(() =>
            {
                return InMemoryData
                    .AsQueryable()
                    .Where(query)
                    .AsEnumerable();
            });
        }

        public bool Any(string merchantId)
        {
            return InMemoryData
             .AsQueryable()
             .Any(x => x.MerchantId == merchantId);
        }

        public int Count(Expression<Func<IFilterView, bool>> query)
        {
            return InMemoryData
                .AsQueryable()
                .Count(query);
        }

        public Task<IFilterView> Get(string id)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<IFilterView> GetAll()
        {
            return InMemoryData;
        }

        public IFilterView GetByMerchantId(string id)
        {
            return InMemoryData
            .AsQueryable()
            .Where(x => x.MerchantId == id).FirstOrDefault();
        }

        public IEnumerable<IFilterView> GetByStatus(string status)
        {
            return InMemoryData.Where(x => x.StatusCode == status);
        }

        public IEnumerable<IFilterView> HasEntriesWith(string ssn, string email, string firstName, string lastName, DateTime dateOfBirth)
        {
            throw new NotImplementedException();
        }

        public void Remove(IFilterView item)
        {
            InMemoryData = InMemoryData.Where(x => x.MerchantId != item.MerchantId).ToList();
        }

        public void Update(string merchantId, IFilterView merchant)
        {
            var item = InMemoryData.FirstOrDefault(x => x.MerchantId == merchantId);
            var index = InMemoryData.IndexOf(item);
            InMemoryData[index] = merchant;
        }

        public Task<IEnumerable<IFilterView>> GetAllUnassigned(IEnumerable<string> roles)
        {
            return Task.Run(() =>
            {
                var unassignes = InMemoryData
                .Where(i => i.Assignees != null && i.Assignees.Any())
                .Where(i => i.Assignees.Keys.Any(a => roles.Contains(a)) == false)
                .Select(i => new FilterView(i))
                .AsEnumerable<IFilterView>().ToList();

                var nullOrEmptyAssignees = InMemoryData
                .Where(i => i.Assignees == null || i.Assignees.Any() == false)
                .Select(i => new FilterView(i));

                unassignes.AddRange(nullOrEmptyAssignees);

                return unassignes.AsEnumerable();
            });
        }
        public Task<IEnumerable<IFilterView>> GetAllAssigned(string userName)
        {
            return Task.Run(() =>
            {
                return InMemoryData.Where(x => x.Assignees.Values.Contains(userName));
            });
        }
        public Task<IEnumerable<IFilterView>> GetUnassignedByStatus(IEnumerable<string> roles, IEnumerable<string> statuses)
        {
            return Task.Run(() =>
            {
                var filteredByStatus = InMemoryData.Where(x => statuses.Contains(x.StatusCode));

                var unassignes = filteredByStatus
                .Where(i => i.Assignees != null && i.Assignees.Any())
                .Where(i => i.Assignees.Keys.Any(a => roles.Contains(a)) == false)
                .Select(i => new FilterView(i))
                .AsEnumerable<IFilterView>().ToList();

                var nullOrEmptyAssignees = filteredByStatus
                .Where(i => i.Assignees == null || i.Assignees.Any() == false)
                .Select(i => new FilterView(i));

                unassignes.AddRange(nullOrEmptyAssignees);

                return unassignes.AsEnumerable();
            });
        }
        public Task<IEnumerable<IFilterView>> GetUnassignedByTags(IEnumerable<string> roles, IEnumerable<string> tags)
        {
            return Task.Run(() =>
            {
                var db = InMemoryData.Where(x => x.Tags != null);
                var filteredByTags = (
                                        from a in db
                                        from b in a.Tags
                                        from c in tags
                                        where b == c
                                        select a
                                     ).Distinct();

                var unassignes = filteredByTags
                .Where(i => i.Assignees != null && i.Assignees.Any())
                .Where(i => i.Assignees.Keys.Any(a => roles.Contains(a)) == false)
                .Select(i => new FilterView(i))
                .AsEnumerable<IFilterView>().ToList();

                var nullOrEmptyAssignees = filteredByTags
                .Where(i => i.Assignees == null || i.Assignees.Any() == false)
                .Select(i => new FilterView(i));

                unassignes.AddRange(nullOrEmptyAssignees);

                return unassignes.AsEnumerable();
            });
        }
        public Task<IEnumerable<IFilterView>> GetAssignedByStatus(string userName, IList<string> statuses)
        {
            return Task.FromResult<IEnumerable<IFilterView>>
            (
                OrderIt(InMemoryData.Where(x => statuses.Contains(x.StatusCode) && x.Assignees.Values.Contains(userName)))
            );
        }
        public Task<IEnumerable<IFilterView>> GetAssignedByTags(string userName, string[] tags)
        {
            return Task.FromResult<IEnumerable<IFilterView>>
            (
                OrderIt(from db in InMemoryData.Where(x => x.Assignees.Values.Contains(userName)).ToList()
                        from dbTags in db.Tags
                        where (from tag in tags select tag).Contains(dbTags)
                        select db).Distinct()
            );
        }

        private IQueryable<IFilterView> OrderIt(IEnumerable<IFilterView> filterView)
        {
            return filterView.OrderBy(x => x.StatusCode)
                             .ThenBy(x => x.Submitted).AsQueryable();
        }

        public Task<IEnumerable<IFilterView>> GetByStatus(IEnumerable<string> statuses)
        {
            return Task.FromResult<IEnumerable<IFilterView>>
            (
                OrderIt(InMemoryData.Where(x => statuses.Contains(x.StatusCode) ))
            );
        }
    }
}