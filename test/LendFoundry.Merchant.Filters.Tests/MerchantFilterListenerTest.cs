﻿using LendFoundry.AssignmentEngine.Client;
using LendFoundry.Configuration.Client;
using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Logging;
using LendFoundry.Merchant.Client;
using LendFoundry.Merchant.Filters.Tests.Fakes;
using LendFoundry.Security.Tokens;
using LendFoundry.StatusManagement;
using LendFoundry.StatusManagement.Client;
using LendFoundry.Tenant.Client;
using Moq;
using System;
using System.Collections.Generic;
using LendFoundry.AssignmentEngine;
using Xunit;

namespace LendFoundry.Merchant.Filters.Tests
{
    public class MerchantFilterListenerTest
    {
        [Fact]
        public void AddFilterEntryWhenAllExecutionOk()
        {
            var _merchantId = "MER0001";
            var statusCode = "M01";
            var statusName = "Initiated";
            var repository = new Mock<IFilterViewRepository>();
            repository.Setup(x => x.AddOrUpdate(It.IsAny<IFilterView>()));
            var statusManagementService = new Mock<IEntityStatusService>();
            statusManagementService.Setup(x =>x.GetStatusByEntity(It.IsAny<string>(),It.IsAny<string>())).Returns(new StatusResponse() {Code = statusCode, Name = statusName });

            var logger = new Mock<ILogger>();
            logger.Setup(x => x.Warn(It.IsAny<string>()));
            var eventhub = new FakeEventHub()
            {
                InMemoryData = new
                {
                    EntityId = _merchantId,
                    NewStatus = "100.03"
                }
            };
            var listener = BuildListener(eventhub, logger.Object, repository.Object, statusManagementService.Object, new Mock<IAssignmentService>().Object);
            listener.Start();

            logger.Verify(x => x.Info($"Merchant status found for this snapshot #{statusCode} #{statusName}"));
        }

        [Fact]
        public void AddFilterEntryWhenMerchantDoesNotExists()
        {
            var _eventName = "MerchantAdded";
            var _merchantId = "MER0002";
            var repository = new Mock<IFilterViewRepository>();
            repository.Setup(x => x.AddOrUpdate(It.IsAny<IFilterView>()));
            var logger = new Mock<ILogger>();
            logger.Setup(x => x.Warn(It.IsAny<string>()));
            var eventhub = new FakeEventHub()
            {
                InMemoryData = new
                {
                    EntityId = _merchantId,
                    NewStatus = "100.03"
                }
            };
            var listener = BuildListener(eventhub, logger.Object, repository.Object);
            listener.Start();

            logger.Verify(x => x.Warn($"Merchant #{_merchantId} could not be found while processing subscription #{_eventName}"));
        }

        [Fact]
        public void AddFilterEntryWhenExceptionRised()
        {
            var _eventName = "MerchantAdded";
            var _merchantId = "MER0001";
            var repository = new Mock<IFilterViewRepository>();
            repository.Setup(x => x.AddOrUpdate(It.IsAny<IFilterView>())).Throws<Exception>();
            var logger = new Mock<ILogger>();
            logger.Setup(x => x.Warn(It.IsAny<string>()));
            var eventhub = new FakeEventHub()
            {
                InMemoryData = new
                {
                    EntityId = _merchantId,
                    NewStatus = "100.03"
                }
            };

            var listener = BuildListener(eventhub, logger.Object, repository.Object);
            listener.Start();
            logger.Verify(x => x.Error($"Unhadled exception while listening event {_eventName}", It.IsAny<Exception>()));
        }

        private IFilterView GetData()
        {
            return new FilterView
            {
                BusinessName = "BusinessName",
                City = "City",
                Contact = "Contact",
                Phone = "Phone",
                State = "State",
                StatusCode = "Status"
            };
        }

        private IMerchantFilterListener BuildListener(IEventHubClient eventHubRef = null, ILogger loggerRef = null, IFilterViewRepository repositoryRef = null, IEntityStatusService statusServiceRef = null, IAssignmentService assignmentServiceRef=  null)
        {
            var emptyReader = It.IsAny<StaticTokenReader>();

            //configuration
            var apiConfigurationFactory = new Mock<LendFoundry.Configuration.Client.IConfigurationServiceFactory<LendFoundry.Merchant.Filters.Configuration>>();
            apiConfigurationFactory.Setup(x => x.Create(emptyReader).Get()).Returns(new Configuration
            {
                Events = new List<EventMapping>
                {
                    new EventMapping
                    {
                        MerchantId = "{Data.EntityId}",
                        Name = "MerchantAdded"
                    }
                }.ToArray()
            });

            //logger
            var loggerFactory = new Mock<ILoggerFactory>();
            if (loggerRef == null)
            {
                var logger = new Mock<ILogger>();
                logger.Setup(x => x.Info(It.IsAny<string>()));
                logger.Setup(x => x.Error(It.IsAny<string>()));
                loggerFactory.Setup(x => x.Create(NullLogContext.Instance)).Returns(logger.Object);
            }
            else
                loggerFactory.Setup(x => x.Create(NullLogContext.Instance)).Returns(loggerRef);

            //eventhub
            var eventHubFactory = new Mock<IEventHubClientFactory>();
            if (eventHubRef == null)
            {
                var eventHubClient = new Mock<IEventHubClient>();
                eventHubClient.Setup(x => x.StartAsync());
                eventHubClient.Setup(x => x.On(It.IsAny<string>(), It.IsAny<Action<EventInfo>>()));
                eventHubFactory.Setup(x => x.Create(It.IsAny<StaticTokenReader>())).Returns(eventHubClient.Object);
            }
            else
                eventHubFactory.Setup(x => x.Create(It.IsAny<StaticTokenReader>())).Returns(eventHubRef);

            //tenant
            var tenantServiceFactory = new Mock<ITenantServiceFactory>();
            var tenantService = new Mock<ITenantService>();
            tenantService.Setup(x => x.GetActiveTenants()).Returns(new List<TenantInfo>
            {
                new TenantInfo { Id = "my-tenant" }
            });
            tenantServiceFactory.Setup(x => x.Create(It.IsAny<StaticTokenReader>())).Returns(tenantService.Object);

            //token
            var tokenHandler = new Mock<ITokenHandler>();
            var token = new Mock<IToken>();
            token.Setup(x => x.Value).Returns("Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJteS1hcHAiLCJpYXQiOiIyMDE1LTEwLTA5VDA1OjExOjI3LjUzNzc3MjdaIiwiZXhwIjpudWxsLCJzdWIiOm51bGwsInRlbmFudCI6Im15LXRlbmFudCIsInNjb3BlIjpudWxsfQ.vVcomx0eqAs8WvFWEB-ntIB8kaijAqEzXB1AC8bNvaM");
            tokenHandler.Setup(x => x.Issue(It.IsAny<string>(), It.IsAny<string>())).Returns(token.Object);

            //repository
            var repositoryFactory = new Mock<IFilterViewRepositoryFactory>();
            if (repositoryRef == null)
            {
                var repository = new Mock<IFilterViewRepository>();
                repository.Setup(x => x.AddOrUpdate(It.IsAny<IFilterView>()));
                repositoryFactory.Setup(x => x.Create(It.IsAny<StaticTokenReader>())).Returns(repository.Object);
            }
            repositoryFactory.Setup(x => x.Create(It.IsAny<StaticTokenReader>())).Returns(repositoryRef);

            //merchant
            var merchantFactory = new Mock<IMerchantServiceFactory>();
            var merchant = new Mock<IMerchantService>();
            merchant.Setup(x => x.Get("MER0001")).Returns(new Merchant
            {
                BusinessAddresses = new[]
                {
                    new BusinessAddress
                    {
                        City = "City",
                        State = "State"
                    }
                },
                Contacts = new[]
                {
                    new Contact
                    {
                        FirstName = "FirstName",
                        LastName = "LastName",
                        PhoneNumber = "PhoneNumber"
                    }
                },
                BusinessName = "BusinessName",
                MerchantId = "MER0001",
                Status = new Status { Code = "100.01" }
            });
            merchantFactory.Setup(x => x.Create(It.IsAny<StaticTokenReader>())).Returns(merchant.Object);

            //status management
            var statusManagementServiceFactory = new Mock<IStatusManagementServiceFactory>();
            if (statusServiceRef == null)
            {
                var statusService = new Mock<IEntityStatusService>();

                statusManagementServiceFactory.Setup(x => x.Create(It.IsAny<StaticTokenReader>())).Returns(statusService.Object);
            }
            else
                statusManagementServiceFactory.Setup(x => x.Create(It.IsAny<StaticTokenReader>())).Returns(statusServiceRef);
            //assignment service
            var assignmentServiceFactory = new Mock<IAssignmentServiceClientFactory>();
            if (assignmentServiceRef == null)
            {
                var assignmentService = new Mock<IAssignmentService>();

                assignmentServiceFactory.Setup(x => x.Create(It.IsAny<StaticTokenReader>())).Returns(assignmentService.Object);
            }
            else
                assignmentServiceFactory.Setup(x => x.Create(It.IsAny<StaticTokenReader>())).Returns(assignmentServiceRef);

            return new MerchantFilterListener
            (
                apiConfigurationFactory.Object,
                tokenHandler.Object,
                eventHubFactory.Object,
                repositoryFactory.Object,
                loggerFactory.Object,
                tenantServiceFactory.Object,
                merchantFactory.Object,
                statusManagementServiceFactory.Object,
                assignmentServiceFactory.Object
            );
        }
    }
}