﻿using System;
using System.Net;
using System.Threading.Tasks;
using LendFoundry.Foundation.Services;
#if DOTNET2
using Microsoft.AspNetCore.Mvc;
#else
using Microsoft.AspNet.Mvc;
#endif

namespace LendFoundry.Partner.Filters.Api.Controllers
{
    /// <summary>
    /// ApiController Class
    /// </summary>
    /// <seealso cref="ExtendedController" />
    [Route("/")]
    public class ApiController : ExtendedController
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ApiController"/> class.
        /// </summary>
        /// <param name="service">The service.</param>
        public ApiController(IPartnerFilterService service)
        {
            Service = service;
        }

        private IPartnerFilterService Service { get; }

        /// <summary>
        /// Gets all.
        /// </summary>
        /// <returns></returns>
        [HttpGet("/all")]
        [ProducesResponseType(typeof(IFilterView), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public IActionResult GetAll()
        {
            return Execute(() => new OkObjectResult(Service.GetAll()));
        }

        /// <summary>
        /// Gets the by status.
        /// </summary>
        /// <param name="status">The status.</param>
        /// <returns></returns>
        [HttpGet("/{status}")]
        [ProducesResponseType(typeof(IFilterView), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public IActionResult GetByStatus(string status)
        {
            status = WebUtility.UrlDecode(status);
            return Execute(() => new OkObjectResult(Service.GetByStatus(status)));
        }

        /// <summary>
        /// Gets all by status.
        /// </summary>
        /// <param name="statuses">The statuses.</param>
        /// <returns></returns>
        [HttpGet("/status/{*statuses}")]
        [ProducesResponseType(typeof(IFilterView), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> GetAllByStatus(string statuses)
        {
            return await ExecuteAsync(async () =>
            {
                var listStatus = statuses?.Split(new[] { '/' }, StringSplitOptions.RemoveEmptyEntries);
                return new OkObjectResult(await Service.GetByStatus(listStatus));
            });
        }
    }
}