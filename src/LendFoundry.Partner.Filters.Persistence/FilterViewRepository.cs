﻿using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Tenant.Client;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.Serializers;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace LendFoundry.Partner.Filters.Persistence
{
    public class FilterViewRepository : MongoRepository<IFilterView, FilterView>, IFilterViewRepository
    {
        static FilterViewRepository()
        {
            BsonClassMap.RegisterClassMap<FilterView>(map =>
            {
                map.AutoMap();
                var type = typeof(FilterView);            
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
                map.SetIsRootClass(true);
            });
            BsonClassMap.RegisterClassMap<Address>(map =>
            {
                map.AutoMap();
                var type = typeof(Address);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
                map.SetIsRootClass(true);
            });
            BsonClassMap.RegisterClassMap<TimeBucket>(map =>
            {
                map.AutoMap();
                map.MapMember(m => m.Time).SetSerializer(new DateTimeOffsetSerializer(BsonType.Document));

                var type = typeof(TimeBucket);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
                map.SetIsRootClass(false);
            });
        }

        public FilterViewRepository(ITenantService tenantService, IMongoConfiguration configuration) :
                base(tenantService, configuration, "partner-filters")
        {
            CreateIndexIfNotExists("partner-id", Builders<IFilterView>.IndexKeys.Ascending(m => m.TenantId).Ascending(i => i.PartnerId), true);
            CreateIndexIfNotExists("partner-status", Builders<IFilterView>.IndexKeys.Ascending(i => i.TenantId).Ascending(i => i.StatusCode));
        }

        public IEnumerable<IFilterView> GetAll()
        {
            return All(x => x.TenantId == TenantService.Current.Id).Result;
        }

        public IEnumerable<IFilterView> GetByStatus(string status)
        {
            return All(x => x.StatusCode == status).Result;
        }

        public void AddOrUpdate(IFilterView view)
        {
            if (view == null)
                throw new ArgumentNullException(nameof(view));

            Expression<Func<IFilterView, bool>> query = l =>
                l.TenantId == TenantService.Current.Id &&
                l.PartnerId == view.PartnerId;

            view.TenantId = TenantService.Current.Id;

            try
            {
                Collection.FindOneAndReplace(query, view, new FindOneAndReplaceOptions<IFilterView, IFilterView> { IsUpsert = true });
            }
            catch (MongoWriteException)
            {
                //Retry one more time, if fail it will throw excepion
                Collection.FindOneAndReplace(query, view, new FindOneAndReplaceOptions<IFilterView, IFilterView> { IsUpsert = true });
            }
        }

        public async Task<IEnumerable<IFilterView>> GetAllUnassigned(IEnumerable<string> roles)
        {
            var tenantId = TenantService.Current.Id;
            var builders = Builders<IFilterView>.Filter;
            var filterByTenantId = builders.Eq(db => db.TenantId, tenantId);
            var filterNotInRoles = builders.AnyNin("Assignees.k", roles);
            var result = await Collection.Find(builders.And(filterByTenantId, filterNotInRoles)).ToListAsync();
            return result;
            //return result.Select(i => new FilterViewUnassigned(i)).AsEnumerable<IFilterViewUnassigned>().ToList();
        }

        public async Task<IEnumerable<IFilterView>> GetAllAssigned(string userName)
        {
            var tenantId = TenantService.Current.Id;
            var builders = Builders<IFilterView>.Filter;
            var filterByTenantId = builders.Eq(db => db.TenantId, tenantId);
            var filterByUserName = builders.AnyIn("Assignees.v", new List<string>() { userName });
            return await Collection.Find(builders.And(filterByTenantId, filterByUserName)).ToListAsync();
        }

        //public async Task<IEnumerable<IFilterView>> GetUnassignedByStatus(IEnumerable<string> roles, IEnumerable<string> statuses)
        //{
        //    var builders = Builders<IFilterView>.Filter;
        //    var filter = builders.AnyNin("Assignees.k", roles);
        //    var tenantId = TenantService.Current.Id;
        //    var selectQuery = Collection.Find(filter).ToEnumerable().AsQueryable().Where(i => i.TenantId == tenantId);

        //    return await Task.FromResult
        //    (
        //        selectQuery
        //            .Where(db => statuses.Contains(db.StatusCode)).ToList()
        //    );
        //}

        public async Task<IEnumerable<IFilterView>> GetUnassignedByTags(IEnumerable<string> roles, IEnumerable<string> tags)
        {
            var builders = Builders<IFilterView>.Filter;
            var filter = builders.AnyNin("Assignees.k", roles);
            var tenantId = TenantService.Current.Id;
            var selectQuery = Collection.Find(filter).ToEnumerable().AsQueryable().Where(i => i.TenantId == tenantId);

            return await Task.FromResult
            (
                selectQuery
                    .Where(db => tags.Any(t => db.Tags.Contains(t))).ToList()
            );
        }

        //public async Task<IEnumerable<IFilterView>> GetAssignedByStatus(string userName, IList<string> statuses)
        //{
        //    var tenantId = TenantService.Current.Id;
        //    var builders = Builders<IFilterView>.Filter;
        //    var filterByTenantId = builders.Eq(db => db.TenantId, tenantId);
        //    var filterByUserName = builders.AnyIn("Assignees.v", new List<string>() { userName });
        //    var filterByStatus = builders.Where(db => statuses.Contains(db.StatusCode));
        //    return await Collection.Find(builders.And(filterByTenantId, filterByUserName, filterByStatus)).ToListAsync();
        //}

        public async Task<IEnumerable<IFilterView>> GetAssignedByTags(string userName, string[] tags)
        {
            var tenantId = TenantService.Current.Id;
            var builders = Builders<IFilterView>.Filter;
            var filterByTenantId = builders.Eq(db => db.TenantId, tenantId);
            var filterByUserName = builders.AnyIn("Assignees.v", new List<string>() { userName });
            var filterByStatus = builders.AnyIn(db => db.Tags, tags);
            return await Collection.Find(builders.And(filterByTenantId, filterByUserName, filterByStatus)).ToListAsync();
        }
        public Task<IEnumerable<IFilterView>> GetByStatus(IEnumerable<string> statuses)
        {
            return Task.FromResult<IEnumerable<IFilterView>>(OrderIt(Query.Where(x => statuses.Contains(x.StatusCode))));
        }
        private IQueryable<IFilterView> OrderIt(IEnumerable<IFilterView> filterView)
        {
            return filterView.OrderBy(x => x.StatusCode)
                .ThenBy(x => x.SubmittedDate).AsQueryable();
        }
    }
}