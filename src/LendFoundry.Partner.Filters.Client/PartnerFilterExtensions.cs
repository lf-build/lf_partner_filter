﻿using System;
using LendFoundry.Security.Tokens;
#if DOTNET2
using Microsoft.Extensions.DependencyInjection;
#else
using Microsoft.Framework.DependencyInjection;
#endif

namespace LendFoundry.Partner.Filters.Client
{
    public static class PartnerFilterExtensions
    {
        [Obsolete("Need to use the overloaded with Uri")]
        public static IServiceCollection AddPartnerFilter(this IServiceCollection services, string endpoint, int port)
        {
            services.AddTransient<IPartnerFilterFactory>(p => new PartnerFilterFactory(p, endpoint, port));
            services.AddTransient(p => p.GetService<IPartnerFilterFactory>().Create(p.GetService<ITokenReader>()));
            return services;
        }

        public static IServiceCollection AddPartnerFilter(this IServiceCollection services, Uri uri)
        {
            services.AddTransient<IPartnerFilterFactory>(p => new PartnerFilterFactory(p, uri));
            services.AddTransient(p => p.GetService<IPartnerFilterFactory>().Create(p.GetService<ITokenReader>()));
            return services;
        }

        public static IServiceCollection AddPartnerFilter(this IServiceCollection services)
        {
            services.AddTransient<IPartnerFilterFactory>(p => new PartnerFilterFactory(p));
            services.AddTransient(p => p.GetService<IPartnerFilterFactory>().Create(p.GetService<ITokenReader>()));
            return services;
        }
    }
}