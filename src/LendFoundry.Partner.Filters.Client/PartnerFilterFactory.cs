﻿using LendFoundry.Foundation.Client;
using LendFoundry.Foundation.Logging;
using LendFoundry.Security.Tokens;
using System;
#if DOTNET2
using Microsoft.Extensions.DependencyInjection;
#else
using Microsoft.Framework.DependencyInjection;
#endif

namespace LendFoundry.Partner.Filters.Client
{
    public class PartnerFilterFactory : IPartnerFilterFactory
    {
        [Obsolete("Need to use the overloaded with Uri")]
        public PartnerFilterFactory(IServiceProvider provider, string endpoint, int port)
        {
            Provider = provider;
            Uri = new UriBuilder("http", endpoint, port).Uri;
        }

        public PartnerFilterFactory(IServiceProvider provider, Uri uri = null)
        {
            Provider = provider;
            Uri = uri;
        }

        private IServiceProvider Provider { get; set; }

        private Uri Uri { get; }

        public IPartnerFilterService Create(ITokenReader reader)
        {
            var uri = Uri;
            if (uri == null)
            {
                var logger = Provider.GetService<ILoggerFactory>().Create(NullLogContext.Instance);
                uri = Provider.GetRequiredService<IDependencyServiceUriResolverFactory>().Create(reader, logger).Get("partner_filters");
            }
            var client = Provider.GetServiceClient(reader, uri);
            return new PartnerFilterClient(client);
        }
    }
}