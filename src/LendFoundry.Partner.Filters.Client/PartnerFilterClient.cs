﻿using LendFoundry.Foundation.Client;
using RestSharp;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Partner.Filters.Client
{
    public class PartnerFilterClient : IPartnerFilterService
    {
        public PartnerFilterClient(IServiceClient client)
        {
            Client = client;
        }

        private IServiceClient Client { get; }

        public IEnumerable<IFilterView> GetAll()
        {
            var request = new RestRequest("/all", Method.GET);
            return Client.Execute<IEnumerable<FilterView>>(request);
        }

        public IEnumerable<IFilterView> GetByStatus(string status)
        {
            var request = new RestRequest("/{status}", Method.GET);
            request.AddUrlSegment("status", status);
            return Client.Execute<IEnumerable<FilterView>>(request);
        }
        public async Task<IEnumerable<IFilterView>> GetByStatus(IEnumerable<string> statuses)
        {
            var request = new RestRequest("/status", Method.GET);
            AppendItemsToRoute(request, statuses);
            return await Client.ExecuteAsync<List<FilterView>>(request);
        }
        public async Task<IEnumerable<IFilterView>> GetAllUnassigned()
        {
            var request = new RestRequest("/unassigned/all", Method.GET);
            return await Client.ExecuteAsync<IEnumerable<FilterView>>(request);
        }
        public async Task<IEnumerable<IFilterView>> GetUnassignedByStatus(IEnumerable<string> statuses)
        {
            var request = new RestRequest("/unassigned/status", Method.GET);
            AppendItemsToRoute(request, statuses);
            return await Client.ExecuteAsync<IEnumerable<FilterView>>(request);
        }
        public async Task<IEnumerable<IFilterView>> GetUnassignedByTags(IEnumerable<string> tags)
        {
            var request = new RestRequest("/unassigned/tag", Method.GET);
            AppendItemsToRoute(request, tags);
            return await Client.ExecuteAsync<IEnumerable<FilterView>>(request);
        }
        public async Task<IEnumerable<IFilterView>> GetAssignedByStatus(IEnumerable<string> statuses)
        {
            var request = new RestRequest("/assigned/status", Method.GET);
            AppendItemsToRoute(request, statuses);
            return await Client.ExecuteAsync<IEnumerable<FilterView>>(request);
        }
        public async Task<IEnumerable<IFilterView>> GetAssignedByTags(IEnumerable<string> tags)
        {
            var request = new RestRequest("/assigned/tag", Method.GET);
            AppendItemsToRoute(request, tags);
            return await Client.ExecuteAsync<IEnumerable<FilterView>>(request);
        }
        public async Task<IEnumerable<IFilterView>> GetAllAssigned()
        {
            var request = new RestRequest("/mine", Method.GET);
            return await Client.ExecuteAsync<IEnumerable<FilterView>>(request);
        }
        private static void AppendItemsToRoute(IRestRequest request, IEnumerable<string> items)
        {
            if (items == null)
                return;

            var itemList = items as IList<string> ?? items.ToList();
            if (!itemList.Any())
                return;

            var url = request.Resource;
            for (var index = 0; index < itemList.Count; index++)
            {
                var tag = itemList[index];
                url = url + $"/{{item{index}}}";
                request.AddUrlSegment($"item{index}", tag);
            }
            request.Resource = url;
        }
    }
}