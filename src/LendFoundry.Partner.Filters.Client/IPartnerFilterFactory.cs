﻿using LendFoundry.Security.Tokens;

namespace LendFoundry.Partner.Filters.Client
{
    public interface IPartnerFilterFactory
    {
        IPartnerFilterService Create(ITokenReader reader);
    }
}