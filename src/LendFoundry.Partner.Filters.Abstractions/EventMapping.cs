﻿namespace LendFoundry.Partner.Filters
{
    public class EventMapping
    {
        public string Name { get; set; }
        public string PartnerId { get; set; }
    }
}