using LendFoundry.Security.Tokens;

namespace LendFoundry.Partner.Filters
{
    public interface IFilterViewRepositoryFactory
    {
        IFilterViewRepository Create(ITokenReader reader);
    }
}