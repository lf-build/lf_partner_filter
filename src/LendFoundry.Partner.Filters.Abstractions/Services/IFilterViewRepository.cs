﻿using LendFoundry.Foundation.Persistence;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace LendFoundry.Partner.Filters
{
    public interface IFilterViewRepository : IRepository<IFilterView>
    {
        IEnumerable<IFilterView> GetAll();

        IEnumerable<IFilterView> GetByStatus(string status);

        void AddOrUpdate(IFilterView view);

        Task<IEnumerable<IFilterView>> GetAllUnassigned(IEnumerable<string> roles);
        Task<IEnumerable<IFilterView>> GetAllAssigned(string userName);
      //  Task<IEnumerable<IFilterView>> GetUnassignedByStatus(IEnumerable<string> roles, IEnumerable<string> statuses);
        //Task<IEnumerable<IFilterView>> GetUnassignedByTags(IEnumerable<string> roles, IEnumerable<string> tags);
        //Task<IEnumerable<IFilterView>> GetAssignedByStatus(string userName, IList<string> statuses);
     //   Task<IEnumerable<IFilterView>> GetAssignedByTags(string userName, string[] tags);

        Task<IEnumerable<IFilterView>> GetByStatus(IEnumerable<string> statuses);
    }
}