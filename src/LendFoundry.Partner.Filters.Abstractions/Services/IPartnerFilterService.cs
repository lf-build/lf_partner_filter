﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace LendFoundry.Partner.Filters
{
    public interface IPartnerFilterService
    {
        IEnumerable<IFilterView> GetAll();

        IEnumerable<IFilterView> GetByStatus(string status);
      
        Task<IEnumerable<IFilterView>> GetAllAssigned();
        Task<IEnumerable<IFilterView>> GetByStatus(IEnumerable<string> statuses);
    }
}