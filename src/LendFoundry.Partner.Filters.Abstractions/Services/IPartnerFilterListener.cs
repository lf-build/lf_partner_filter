﻿namespace LendFoundry.Partner.Filters
{
    public interface IPartnerFilterListener
    {
        void Start();
    }
}