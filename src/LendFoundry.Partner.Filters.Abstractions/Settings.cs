﻿using System;

namespace LendFoundry.Partner.Filters
{
    public static class Settings
    {
        public static string ServiceName => Environment.GetEnvironmentVariable($"CONFIGURATION_NAME") ?? "partner-filters";
    }
}