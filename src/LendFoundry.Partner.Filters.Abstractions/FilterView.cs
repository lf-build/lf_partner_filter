﻿using System.Collections.Generic;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Persistence;
using System;
using Newtonsoft.Json;
using LendFoundry.Foundation.Client;

namespace LendFoundry.Partner.Filters
{
    public class FilterView : Aggregate, IFilterView
    {
        public FilterView() { }
        public FilterView(IFilterView filterView)
        {
            
            BusinessName = filterView.BusinessName;         
            PhoneNumber = filterView.PhoneNumber;      
            PartnerId = filterView.PartnerId;
            Id = filterView.PartnerId;
            SubmittedDate = filterView.SubmittedDate;        
            Tags = filterView.Tags;               
            OwnerFirstName = filterView.OwnerFirstName;
            OwnerLastName = filterView.OwnerLastName;
            OwnerDateOfBirth = filterView.OwnerDateOfBirth;
            SSN = filterView.SSN;
            OwnerAddress = filterView.OwnerAddress;
            OwnedPercentage = filterView.OwnedPercentage;
            Line1 = filterView.Line1;
            Line2 = filterView.Line2;
            Line3 = filterView.Line3;          
            Type = filterView.Type;
            City = filterView.City;
            State = filterView.State;           
            Country = filterView.Country;
            UserId = filterView.UserId;
            ZipCode = filterView.ZipCode;
            NickName = filterView.NickName;
            PartnerType = filterView.PartnerType;
            IsPrimary = filterView.IsPrimary;
            JobTitle = filterView.JobTitle;
            PhoneExt = filterView.PhoneExt;
            Email = filterView.Email;
            Notes = filterView.Notes;
            StatusCode = filterView.StatusCode;
            StatusChangedBy = StatusChangedBy;
            StatusChangeDate = StatusChangeDate;
            EIN = EIN;
            WebSite = WebSite;           

        }

        public string PartnerId { get; set; }
        public string BusinessName { get; set; }
        
     
        public string City { get; set; }
        public string State { get; set; }

        public string UserId { get; set; }
        public string ZipCode { get; set; }    
            
        public IEnumerable<string> Tags { get; set; }
       

        public string NickName{ get;set;}

        public string PartnerType { get; set; }
        public string Line1{get;set; }

        public string Line2 { get; set; }
       

        public string Line3 { get; set; }
        public string Type { get; set; }
        

        public string Country { get; set; }
        

        public bool IsPrimary { get; set; }
       
        public string ContactId { get; set; }
      

        public string FirstName { get; set; }
      

        public string LastName { get; set; }
       

        public string JobTitle { get; set; }
      

        public string PhoneNumber { get; set; }
        

        public string PhoneExt { get; set; }

        public string Email { get; set; }

        public DateTime? DateOfBirth { get; set; }

        public string StatusCode { get; set; }

        public string StatusChangedBy { get; set; }

        public string Notes { get; set; }

        public string OwnerFirstName { get; set; }

        public string OwnerLastName { get; set; }

        public DateTime? OwnerDateOfBirth { get; set; }

        public string SSN { get; set; }

        public string OwnedPercentage { get; set; }

        [JsonConverter(typeof(InterfaceConverter<IAddress, Address>))]
        public IAddress OwnerAddress { get; set; }

        public TimeBucket SubmittedDate { get; set; }

        public TimeBucket StatusChangeDate { get; set; }

        public string EIN { get; set; }

        public string WebSite { get; set; }


    }
}