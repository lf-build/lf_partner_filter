﻿using LendFoundry.Foundation.Client;
using System.Collections.Generic;

namespace LendFoundry.Partner.Filters
{
    public class Configuration : IDependencyConfiguration
    {
        public EventMapping[] Events { get; set; }
        public Dictionary<string, string> Dependencies { get; set; }
        public string ConnectionString { get; set; }
        public string Database { get; set; }

    }
}