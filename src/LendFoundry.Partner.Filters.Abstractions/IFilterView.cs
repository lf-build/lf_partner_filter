﻿using System.Collections.Generic;
using LendFoundry.Foundation.Persistence;
using System;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Client;
using Newtonsoft.Json;

namespace LendFoundry.Partner.Filters
{
    public interface IFilterView : IAggregate
    {
        string PartnerId { get; set; }
        string BusinessName { get; set; }
        string NickName { get; set; }

        string PartnerType { get; set; }      
     
        string Line1 { get; set; }
        string Line2 { get; set; }
        string Line3 { get; set; }
  
        string Type { get; set; }
        string City { get; set; }
        string Country { get; set; }
        bool IsPrimary { get; set; }
       
        string ZipCode { get; set; }
        string State { get; set; }       

        string ContactId { get; set; }
        string FirstName { get; set; }
        string LastName { get; set; }
        string JobTitle { get; set; }
        string PhoneNumber { get; set; }
        string PhoneExt { get; set; }   
        string Email { get; set; }
        DateTime? DateOfBirth { get; set; }
        string UserId { get; set; }

        string StatusCode { get; set; }
        string StatusChangedBy { get; set; }
        string Notes { get; set; }

        string OwnerFirstName { get; set; }
        string OwnerLastName { get; set; }
        DateTime? OwnerDateOfBirth { get; set; }

        string SSN { get; set; }

        string OwnedPercentage { get; set; }

        [JsonConverter(typeof(InterfaceConverter<IAddress, Address>))]
        IAddress OwnerAddress { get; set; }

        TimeBucket SubmittedDate { get; set; }       
        TimeBucket StatusChangeDate { get; set; }
        IEnumerable<string> Tags { get; set; }

         string EIN { get; set; }

         string WebSite { get; set; }


    }
}