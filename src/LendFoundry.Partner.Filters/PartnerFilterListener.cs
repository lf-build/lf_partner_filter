﻿using LendFoundry.Configuration;
using LendFoundry.EventHub;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Services;
using LendFoundry.Foundation.Listener;
using LendFoundry.Security.Tokens;
using LendFoundry.Tenant.Client;
using System;
using System.Linq;
using LendFoundry.Partner.Client;
using System.Collections.Generic;

namespace LendFoundry.Partner.Filters
{
    public class PartnerFilterListener : ListenerBase, IPartnerFilterListener
    {
        public PartnerFilterListener
        (
            IConfigurationServiceFactory configurationFactory,            
            ITokenHandler tokenHandler,
            IEventHubClientFactory eventHubFactory,
            IFilterViewRepositoryFactory repositoryFactory,
            ILoggerFactory loggerFactory,
            ITenantServiceFactory tenantServiceFactory,
            IPartnerServiceFactory partnerServiceFactory)
            : base(tokenHandler, eventHubFactory, loggerFactory, tenantServiceFactory, Settings.ServiceName)
        {
            if (partnerServiceFactory == null) throw new ArgumentException($"{nameof(partnerServiceFactory)} is mandatory");
            if (configurationFactory == null) throw new ArgumentException($"{nameof(configurationFactory)} is mandatory");
            if (tokenHandler == null) throw new ArgumentException($"{nameof(tokenHandler)} is mandatory");
            if (eventHubFactory == null) throw new ArgumentException($"{nameof(eventHubFactory)} is mandatory");
            if (loggerFactory == null) throw new ArgumentException($"{nameof(loggerFactory)} is mandatory");
            if (tenantServiceFactory == null) throw new ArgumentException($"{nameof(tenantServiceFactory)} is mandatory");
            if (repositoryFactory == null) throw new ArgumentException($"{nameof(repositoryFactory)} is mandatory");
       
            EventHubFactory = eventHubFactory;
            ConfigurationFactory = configurationFactory;
            TokenHandler = tokenHandler;
            RepositoryFactory = repositoryFactory;
            LoggerFactory = loggerFactory;
            TenantServiceFactory = tenantServiceFactory;
            PartnerServiceFactory = partnerServiceFactory;
        }

        private IConfigurationServiceFactory ConfigurationFactory { get; }
        private IPartnerServiceFactory PartnerServiceFactory { get; }
        private IEventHubClientFactory EventHubFactory { get; }
        private ITenantServiceFactory TenantServiceFactory { get; }
        private ITokenHandler TokenHandler { get; }
        private IFilterViewRepositoryFactory RepositoryFactory { get; }
        private ILoggerFactory LoggerFactory { get; }

        public override List<string> GetEventNames(string tenant)
        {
            var token = TokenHandler.Issue(tenant, Settings.ServiceName);
            var reader = new StaticTokenReader(token.Value);
            var configuration = ConfigurationFactory.Create<Configuration>(Settings.ServiceName, reader).Get();
            if (configuration == null || configuration.Events == null|| !configuration.Events.Any())
                return null;
            else
            {
                return configuration.Events.Select(x=>x.Name).Distinct().ToList<string>();
            }
        }
        
        public override List<string> SubscribeEvents(string tenant, ILogger logger)
        {
            var serviceName = Settings.ServiceName;
            var token = TokenHandler.Issue(tenant, serviceName);
            var reader = new StaticTokenReader(token.Value);
            var eventhub = EventHubFactory.Create(reader);          
            var configuration = ConfigurationFactory.Create<Configuration>(Settings.ServiceName, reader).Get();
            var repository = RepositoryFactory.Create(reader);
            var partnerService = PartnerServiceFactory.Create(reader);

            if (configuration == null)
            {
                logger.Info($"The configuration for service #{serviceName} could not be found for {tenant}, please verify");
                return null;
            }
            else
            {
                    var uniqueEvents = configuration.Events.Distinct().ToList();
   
                    logger.Info($"Total of ({uniqueEvents.Count}) rules found for tenant {tenant}");

                    uniqueEvents.ToList()
                                .ForEach(eventConfig => eventhub.On(eventConfig.Name, AddView(eventConfig, repository, logger, partnerService)));

                    return uniqueEvents.Select(x=>x.Name).Distinct().ToList<string>();
            }
        }

        private static Action<EventInfo> AddView(EventMapping eventConfiguration, IFilterViewRepository repository, ILogger logger, IPartnerService partnerService)
        {
             return @event =>
            {
                logger.Info($"Processing {@event.Name} with Id - {@event.Id} for {@event.TenantId}");

                var PartnerId = eventConfiguration.PartnerId.FormatWith(@event);
                try
                {
                    var partner = partnerService.Get(PartnerId).Result;
                    if (partner != null)
                    {
                        var contact = partner.Contacts?.FirstOrDefault(x => x.IsPrimary);
                        var filterView = new FilterView()
                        {
                            BusinessName = partner.Name,
                            PhoneNumber = contact?.PhoneNumber,
                            PartnerId = partner.PartnerId,
                            SubmittedDate = partner.SubmittedDate,
                            OwnerFirstName = partner.PrincipalOwner?.FirstName,
                            OwnerLastName = partner.PrincipalOwner?.LastName,
                            OwnerDateOfBirth = partner.PrincipalOwner?.DateOfBirth,
                            OwnedPercentage = partner.PrincipalOwner?.OwnedPercentage,
                            OwnerAddress = partner.PrincipalOwner?.Address,
                            SSN = partner.PrincipalOwner?.SSN,
                            Line1 = partner.Address?.Line1,
                            Line2 = partner.Address?.Line2,
                            Line3 = partner.Address?.Line3,
                            Type = partner.Address?.Type.ToString(),
                            City = partner.Address?.City,
                            State = partner.Address?.State,
                            Country = partner.Address?.Country,
                            UserId = contact?.UserId,
                            ZipCode = partner.Address?.ZipCode,
                            NickName = partner.NickName,
                            PartnerType = Convert.ToString(partner.PartnerType),
                            IsPrimary = partner.Address.IsPrimary,
                            JobTitle = contact?.JobTitle,
                            PhoneExt = contact?.PhoneExt,
                            Email = contact?.Email,
                            Notes = partner.Notes,
                            StatusCode = partner.Status,
                            StatusChangedBy = partner.StatusChangedBy,
                            FirstName = contact?.FirstName,
                            LastName = contact?.LastName,
                            ContactId = contact?.ContactId,
                            DateOfBirth = contact?.DateOfBirth,
                            StatusChangeDate = partner.StatusChangeDate,
                            EIN = partner.EIN,
                            WebSite = partner.WebSite
                        };


                        repository.AddOrUpdate(filterView);
                        logger.Info($"New snapshot added to partner {PartnerId}");
                    }
                    else
                        logger.Warn($"Partner #{PartnerId} could not be found while processing subscription #{@event.Name}", @event);

                }
                catch (Exception ex)
                {
                    logger.Error($"Unhadled exception while listening event {@event.Name}", ex, @event);
                }
            };
        }    
    }
}