﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LendFoundry.Foundation.Services;
using LendFoundry.Security.Tokens;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Date;

namespace LendFoundry.Partner.Filters
{
    public class PartnerFilterService : IPartnerFilterService
    {
        public PartnerFilterService
        (
            IFilterViewRepository repository, ITokenReader tokenReader, ITokenHandler tokenParser,ILogger logger,ITenantTime tenantTime)
        {
            Repository = repository;
            TokenReader = tokenReader;
            TokenParser = tokenParser;
            Logger = logger;
            TenantTime = tenantTime;
        }

        private IFilterViewRepository Repository { get; }

        private ITokenReader TokenReader { get; }

        private ITokenHandler TokenParser { get; }       

        private ILogger Logger { get; }
        private ITenantTime TenantTime { get; }
        public IEnumerable<IFilterView> GetAll()
        {
            return Repository.GetAll();
        }

        public IEnumerable<IFilterView> GetByStatus(string status)
        {
            if (string.IsNullOrWhiteSpace(status))
                throw new ArgumentException($"{nameof(status)} is mandatory");
            try
            {
                return Repository.GetByStatus(status);

            }catch(Exception exception)
            {
                Logger.Error("Error While Processing GetByStatus Date & Time:" + TenantTime.Now + "Service: PartnerFilter" + "Exception" + exception.Message);
                throw;
            }
        }


        public Task<IEnumerable<IFilterView>> GetAllAssigned()
        {
            var userName = GetTokenUserName();

            if (string.IsNullOrWhiteSpace(userName))
                throw new ArgumentException("Token user cannot be found in the request, please verify");
            try
            { 
              return Repository.GetAllAssigned(userName);
            }
            catch (Exception exception)
            {
                Logger.Error("Error While Processing GetByStatus Date & Time:" + TenantTime.Now + "Service: PartnerFilter" + "Exception" + exception.Message);
                throw;
            }
        }

       
        private string GetTokenUserName()
        {
            var token = TokenParser.Parse(TokenReader.Read());
            return token.Subject;
        }
        public Task<IEnumerable<IFilterView>> GetByStatus(IEnumerable<string> statuses)
        {

            if (statuses == null || !statuses.Any())
                throw new ArgumentException($"{nameof(statuses)} is mandatory");
            try
            {
                var statusList = statuses as IList<string> ?? statuses.ToList();
                return Repository.GetByStatus(statusList);
            }
            catch (Exception exception)
            {
                Logger.Error("Error While Processing GetByStatus Date & Time:" + TenantTime.Now + "Service: PartnerFilter" + "Exception" + exception.Message);
                throw;
            }
}
    }
}