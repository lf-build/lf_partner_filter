﻿#if DOTNET2
using Microsoft.Extensions.DependencyInjection;
using Microsoft.AspNetCore.Builder;
#else
using Microsoft.Framework.DependencyInjection;
using Microsoft.AspNet.Builder;
#endif

namespace LendFoundry.Partner.Filters
{
    public static class PartnerFilterListenerExtensions
    {
        public static void UsePartnerFilterListener(this IApplicationBuilder app)
        {
            app.ApplicationServices.GetRequiredService<IPartnerFilterListener>().Start();
        }
    }
}